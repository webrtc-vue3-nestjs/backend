import { UserDto } from 'src/users/dto/user.dto'

export class LoginResponseDto {
  readonly user: UserDto
  readonly token: string
}

import { ApiProperty } from '@nestjs/swagger'
import { IsString, Length, IsEmail } from 'class-validator'

export class LoginRequestDto {
  @ApiProperty({ example: 'gotka_gva@mail.ru', description: 'E-mail пользователя' })
  @IsEmail({}, { message: 'Должно быть email формата' })
  @IsString({ message: 'Должно быть строкой' })
  readonly email: string

  @ApiProperty({ example: 'heroes43081', description: 'Пароль пользователя' })
  @IsString({ message: 'Должно быть строкой' })
  @Length(4, 16, { message: 'Не меньше 4 и не больше 16' })
  readonly password: string
}

import { HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { UsersService } from 'src/users/users.service'
import * as bcrypt from 'bcryptjs'
import { LoginRequestDto } from './dto/login-request.dto'
import { LoginResponseDto } from './dto/login-response.dto'
import { UserDto } from 'src/users/dto/user.dto'

@Injectable()
export class AuthService {
  constructor(private userService: UsersService, private jwtService: JwtService) {}

  async login(userDto: LoginRequestDto): Promise<LoginResponseDto> {
    const user = await this.userService.getUserByEmailWithoutPassword(userDto.email)
    return {
      user,
      token: this.generateToken(user),
    }
  }

  async register(userDto: UserDto): Promise<LoginResponseDto> {
    const isUserFound = await this.userService.checkUniqueUser(userDto.email, userDto.login)
    if (isUserFound) {
      throw new HttpException('Пользователь с таким логином или e-mail уже существует', HttpStatus.BAD_REQUEST)
    }
    const salt = bcrypt.genSaltSync(10)
    const hashPassword = await bcrypt.hashSync(userDto.password, salt)
    const user = await this.userService.createUser({
      ...userDto,
      password: hashPassword,
    })

    return {
      user,
      token: this.generateToken(user),
    }
  }

  private generateToken(user) {
    const payload = { email: user.email }
    return this.jwtService.sign(payload)
  }

  async validateUser(userDto: LoginRequestDto): Promise<UserDto> {
    const user = await this.userService.getUserByEmail(userDto.email)
    if (!user) throw new UnauthorizedException({ message: 'Email не найден' })

    const passwordEquals = await bcrypt.compare(userDto.password, user.password)

    if (passwordEquals) return user
    throw new UnauthorizedException({
      message: 'Неверный пароль',
    })
  }
}

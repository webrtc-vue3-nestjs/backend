import { Body, Controller, Post, UseGuards } from '@nestjs/common'
import { ApiBody, ApiOperation, ApiResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger'
import { UserDto } from 'src/users/dto/user.dto'
import { AuthService } from './auth.service'
import { LoginRequestDto } from './dto/login-request.dto'
import { LoginResponseDto } from './dto/login-response.dto'
import { LocalAuthGuard } from './guards/local-auth.guard'

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/login')
  @UseGuards(LocalAuthGuard)
  @ApiOperation({ summary: 'Аутентификация пользователя' })
  @ApiResponse({ status: 200, type: LoginResponseDto })
  @ApiBody({ type: LoginRequestDto })
  @ApiUnauthorizedResponse({ description: 'Неверные входные данные' })
  public async login(@Body() user: LoginRequestDto): Promise<LoginResponseDto> {
    return this.authService.login(user)
  }

  @Post('/register')
  @ApiOperation({ summary: 'Регистрация пользователя' })
  @ApiResponse({ status: 200, type: LoginResponseDto })
  @ApiUnauthorizedResponse({ description: 'Неверные входные данные' })
  @ApiBody({ type: UserDto })
  public async register(@Body() user: UserDto): Promise<LoginResponseDto> {
    return this.authService.register(user)
  }
}

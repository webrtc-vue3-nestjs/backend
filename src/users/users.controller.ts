import { Headers, Controller, Get, UseGuards, UsePipes } from '@nestjs/common'
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger'
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard'
import { ValidationPipe } from 'src/pipes/validation.pipe'
import { UserDto } from './dto/user.dto'
import { User } from './user.model'
import { UsersService } from './users.service'

@Controller('users')
@ApiTags('Users')
export class UsersController {
  constructor(private userService: UsersService) {
    this.userService = userService
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Получение всех пользователей' })
  @ApiResponse({ status: 200, type: [User] })
  @UsePipes(ValidationPipe)
  @Get()
  public async getAllUsers(): Promise<UserDto[]> {
    return this.userService.getAllUsers()
  }

  @Get('/by-token')
  public getUserByToken(@Headers() headers): Promise<UserDto> {
    return this.userService.getUserByToken(headers.authorization)
  }
}

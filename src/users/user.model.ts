import { ApiProperty } from '@nestjs/swagger'
import { Column, DataType, Model, Table } from 'sequelize-typescript'

interface UsersAttrs {
  name: string
  login: string
  email: string
  password?: string
}

@Table({ tableName: 'users' })
export class User extends Model<User, UsersAttrs> {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number

  @ApiProperty({ example: 'Иван', description: 'Имя пользователя' })
  @Column({ type: DataType.STRING, allowNull: false })
  name: string

  @ApiProperty({ example: 'CoBPeMeHHuK', description: 'Уникальный логин пользователя' })
  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  login: string

  @ApiProperty({ example: 'email@mail.ru', description: 'E-mail пользователя' })
  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  email: string

  @ApiProperty({ example: 'password1234', description: 'Пароль пользователя' })
  @Column({ type: DataType.STRING, allowNull: true })
  password: string
}

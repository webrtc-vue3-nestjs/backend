import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { UserDto } from './dto/user.dto'
import { Op } from 'sequelize'
import jwt_decode from 'jwt-decode'

import { User } from './user.model'

@Injectable()
export class UsersService {
  constructor(@InjectModel(User) private userRepository: typeof User) {}
  async createUser(dto: UserDto): Promise<UserDto> {
    return await this.userRepository.create(dto)
  }

  async getAllUsers(): Promise<UserDto[]> {
    return await this.userRepository.findAll({ attributes: { exclude: ['password'] } })
  }

  async getUserByEmail(email: string): Promise<UserDto> {
    return await this.userRepository.findOne({
      raw: true,
      where: { email },
      include: { all: true },
    })
  }

  async getUserByEmailWithoutPassword(email: string): Promise<UserDto> {
    const user = await this.userRepository.findOne({
      raw: true,
      where: { email },
      include: { all: true },
    })
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...other } = user
    return {
      ...other,
    }
  }

  async getUserByToken(token: string): Promise<UserDto> {
    const { email } = jwt_decode<UserDto>(token)
    return await this.getUserByEmailWithoutPassword(email)
  }

  async checkUniqueUser(email: string, login: string): Promise<boolean> {
    const user = await this.userRepository.findOne({
      raw: true,
      where: {
        [Op.or]: [{ email }, { login }],
      },
      include: { all: true },
    })
    return !!user
  }
}

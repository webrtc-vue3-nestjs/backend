import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { AppModule } from './app.module'

async function bootstrap() {
  const PORT = process.env.PORT || 7000
  const app = await NestFactory.create(AppModule)

  app.setGlobalPrefix('/api')

  const config = new DocumentBuilder()
    .setTitle('Мессенджер')
    .setDescription('Документация REST API')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build()
  const document = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup('docs', app, document)

  await app.listen(PORT, () => {
    console.log(`Sever is started on port ${PORT}`)
  })
}
bootstrap()
